package app;

import java.util.Scanner;

public class App {
	public static void main(String[] args) throws Exception {

		// ! create some instance
		RsaFunctions main = new RsaFunctions(); // * for important rsa functions

		MsgConverter convert = new MsgConverter(); // * to en- and decode input message

		Helper basics = new Helper(); // * to get N, phiN

		System.out.println(">>>>>>>>>> WELCOME <<<<<<<<<<");

		Scanner input = new Scanner(System.in); // * for input stream

		// ! input message
		// ask for input text until length > 0
		String inpMsg;
		do {
			System.out.println("Type here your text to en- or decode: ");
			inpMsg = input.nextLine();
		} while (inpMsg.toCharArray().length < 1);

		// ! choose mode
		// * if: mode = 1 -> encode;
		// * else if: mode = 2 -> decode
		int mode = 0;
		do {
			System.out.print("1: Encode \n2: Decode\n Choose your mode: ");
			mode = input.nextInt();
		} while (mode != 1 && mode != 2);

		// ! create Key Pair / own keys
		// * give choice to [1]create a new key pair or [2]use own keys
		System.out.print("Do you want to create a new key pair [1] yes; [2] no: ");
		int praivetKi = 0;
		int publicKi = 0;
		int[] N_phiN = new int[2];
		int key_mode = 1;

		do { // * ask until mode is 1 or 2
			key_mode = input.nextInt();
		} while (key_mode < 1 || key_mode > 2);

		if (key_mode == 2) { // * if: use own keys
			System.out.print("Private Key: "); // private Key
			praivetKi = input.nextInt();
			System.out.print("Public Key: "); // public Key
			publicKi = input.nextInt();
			System.out.print("Modulo: "); // N
			N_phiN[0] = input.nextInt();

			// * output key pair
			System.out.println("Private Key: {" + praivetKi + " | " + N_phiN[0] + "}");
			System.out.println("Public Key: {" + publicKi + " | " + N_phiN[0] + "}");
		} else { // * if: create a new key pair
					// ! get N and phi(N)
			N_phiN = basics.getBasics(); // N

			// ! gen random private Key
			praivetKi = main.get_praivetKi(N_phiN[0], N_phiN[1]); // private Key
			// ! gen random public Key
			publicKi = main.get_publicKi(praivetKi, N_phiN[1]); // public Key

			// * output key pair
			System.out.println("Private Key { " + praivetKi + " | " + N_phiN[0] + " }");
			System.out.println("Public Key: { " + publicKi + " | " + N_phiN[0] + " }");
		}

		// * close Scanner instance
		input.close();

		// ! en- or decode like you choose
		String output = "";
		if (mode == 1)
			output = convert.StrToInt(inpMsg, publicKi, N_phiN[0]); // * if: mode = 1 -> convert msg to int[]
		else if (mode == 2)
			output = convert.IntToStr(inpMsg, praivetKi, N_phiN[0]); // * else if: mode = 2 -> convert int[] to msg

		// ! output result
		if (output.toCharArray().length > 0) {
			System.out.println("Output: " + output); // output encoded or decoded message
		} else {
			System.out.println("There is no output."); // if however no output available
		}
	}
}