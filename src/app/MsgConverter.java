package app;

class MsgConverter {
    // ! helper instance
    Helper helper = new Helper();

    boolean debug = false;

    public String StrToInt(String Msg, int publicKey, int N) {
        String[] a = Msg.split("");

        if (debug) // > output char as integer
            for (String Z : a) {
                System.out.print(Z + ": " + (int) Z.charAt(0) + "; ");
            }

        String res = "";
        for (String i : a) {
            res += (Integer.toString(helper.AmulBmodC((int) i.charAt(0), publicKey, N)) + " ");
        }
        return res;
    }

    public String IntToStr(String Msg, int privateKey, int N) {
        String[] a = Msg.split(" ");
        
        if (debug) // > output as single char
            for (String z : a) {
                System.out.print(z + " ");
            }

        String res = "";
        for (String i : a) {
            res += (char) (helper.AmulBmodC(Integer.parseInt(i), privateKey, N));
        }
        return res;
    }

}